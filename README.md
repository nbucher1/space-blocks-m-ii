# SPACE BLOCKS M - II 

### Project Summary  

Space Blocks M - II is a project that I've been working on during my school breaks. It's a story driven tower defense game for Android built with Unity.  

### Story 

The year is 2096, and humanity is in trouble...

Five years ago, an army of evil aliens we call Galagmanauts arrived with a plan to destroy all life in our Solar System.

Earth put up a fight but quickly fell. Human settlements on the Moon and Mars were destroyed soon after.

The few humans who survived the invasion were forced to retreat into settlements in the outer regions of the Asteroid Belt. There they built defences, readying themselves for the next attacks.

The last remaining settlement, Ursius-6, is where our story begins...


### Setup  

1. Download repository
2. Set the default screen size to a mobile device's screen size (I used Samsung JV3)
3. Any build errors might be due to having to import standard assets (click "assets->import asset->x" where x is the asset you need. Or, if that doesn't work, click "window->asset store" and then search for, download, and import "Unity Standard Assets").

### Contact  
*  If you have any questions or would like to get involved, contact me by email: _admin@nicholasbucher.com_