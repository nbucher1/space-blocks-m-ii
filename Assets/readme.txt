//--[ Installed packages ]--// 

GameSceneController Package
-------------------------------

TODO:
- make a task list for completing the entire battle system
	-- be able to place fences across paths only
	-- have galagmanauts stop when they hit a fence (or have the fence attack the galagmanaut that hits it)
	//-- have build-crosshair follow mouse anywhere on buildable collider when building a non-fence
		--- if building a fence, have the path be highlighted  
		--- if not building a fence, have the non-path map sections be highlighted  
	-- have aim-crosshair follow mouse anywhere when aiming building 

- create intro/title scene
	- on the intro text
		- glowing sides, extra glow on press
		- art covering the screen on each panel
	- on the intro dialogue
		- make concept art and dialogue that leads into an encounter. don't start


- make what happens when the game starts + tutorial 
	-- List what you want to be in the tutorial
	-- Make example scene with tutorial
	-- Make intro/tutorial script

- Achievements
	-- set up an achievement system that is linked to in the player class (similar or within playerhistory)
	-- add button on myhome to view them
	-- store a count of enemy types killed in the playerhistory class (for past levels and for this level)

- write the first level dialogue and messages


teeth play do