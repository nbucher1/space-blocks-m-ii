﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;
using System;

namespace GSC
{
    public delegate void MyDelegate();
    public delegate IEnumerator CoroutineDelegate();

    public class MenuUtility : GameElement
    {
        public GameObject menuBGObj;
        public Font gameFont;

        Stack<Menu> menuStack;

        // Use this for initialization
        new void Awake()
        {
            base.Awake();
            menuStack = new Stack<Menu>();
            if (gameFont == null) Debug.LogError("Please set gameFont in MenuUtility");
        }


        public Menu CreateEmptyMenu(float x = 0, float y = 0, Vector2? anchorMin = null, Vector2? anchorMax = null)
        {
            //if not specified, the anchors will be at the center of the screen
            if (anchorMax == null) anchorMax = new Vector2(.5f, .5f);
            if (anchorMin == null) anchorMin = new Vector2(.5f, .5f);
            //instantiate the menu prefab object
            var menuObj = Instantiate(menuBGObj, Vector2.zero, Quaternion.identity) as GameObject;
            //set the anchors
            var mTrans = menuObj.GetComponent<RectTransform>();
            mTrans.anchorMin = anchorMin.Value;
            mTrans.anchorMax = anchorMax.Value;
            //set the parent of the menu to the canvas
            mTrans.SetParent(gsc.canvas.transform);
            //set it below the inventory if it is playing, otherwise set it below the transition
            if(gsc.curGameState == GameSceneController.GameState.PLAYING)
            {
                //if there's a popup showing, this is the index it should be at so that it appears below the inventory screen
                mTrans.SetSiblingIndex(mTrans.GetSiblingIndex() - 3);
            }
            else
            {
                //this is the index below the transition
                mTrans.SetSiblingIndex(mTrans.GetSiblingIndex() - 1);
            }
            var menu = menuObj.gameObject.GetComponent<Menu>();
            //set the position (do this after setting the anchors)
            mTrans.anchoredPosition = new Vector2(x, y);
            //make sure this menu has started
            menu.Awake();
            //push this menu onto the menuStack
            menuStack.Push(menu);
            return menu;
        }       
    }
}