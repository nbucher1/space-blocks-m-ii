﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

namespace GSC
{
    public class PopupController : MonoBehaviour
    {
        GameSceneController gsc;
        List<Menu> popupMenus;



        // Use this for initialization
        void Awake()
        {
            gsc = GameObject.Find("GameSceneController").GetComponent<GameSceneController>();
            popupMenus = new List<Menu>();
        }



        void Update()
        {
            //if we are showing a popup box
            if(popupMenus.Count > 0)
            {
                //if the A button is pressed, the popup has been given time to appear, and the game is not paused
                if (DynamicInput.IsPressed_P1(ButtonType.A) && gsc.curGameState != GameSceneController.GameState.PAUSED)
                {
                    //hide the popup box
                    HidePopup();
                }
                //else if we are on the inventory screen and anywhere on the screen has been clicked
                else if(Input.GetMouseButtonDown(0) && gsc.curGameState != GameSceneController.GameState.PAUSED)
                {
                    //hide the popup box
                    HideAllPopups();
                }   
            }
        }



        public void ShowPopup(string message, float x = 0, float y = -36, Color? c = null, bool dim = false)
        {
            //uses a coroutine to wait a frame for the debounce of the button press
            //(so it doesn't immediately register that you pressed A to close the popup)
            StartCoroutine(ShowPopupCoroutine(message, x, y, c, dim));
        }
        IEnumerator ShowPopupCoroutine(string message, float x, float y, Color? c, bool dim)
        {
            //wait a frame for the debounce
            yield return new WaitForEndOfFrame();
            //dim the screen
            if (dim)
            {
                gsc.transition.DimScreen();
                if(c == null)
                {
                    c = Color.white;
                }
            }
            else if(c == null)
            {
                c = new Color(.01f, .1f, .4f);
            }
            ////lock the player
            //if (gsc.player != null)
            //{
            //    gsc.player.LockMovement();
            //    gsc.player.LockAttack();
            //}
            //create the new popup menu
            var newMenu = gsc.menuUtility.CreateEmptyMenu(x, y, new Vector2(.5f, 1), new Vector2(.5f, 1));
            newMenu.AddText(message, 26, color: c);
            popupMenus.Insert(0, newMenu);
        }



        public void HidePopup()
        {
            StartCoroutine(HidePopupCoroutine());
        }
        IEnumerator HidePopupCoroutine()
        {
            //wait a frame for the debounce
            yield return new WaitForEndOfFrame();
            //undim the screen
            gsc.transition.UndimScreen();
            //destroy the first popupmenu in the queue(really a list acting as a queue)
            Destroy(popupMenus.Last().gameObject);
            popupMenus.Remove(popupMenus.Last());
        }
        public void HideAllPopups()
        {
            for(int i=0; i<popupMenus.Count; i++)
            {
                HidePopup();
            }
        }



        public void ShowPopupThenDo(string message, MyDelegate d, float x = 0, float y = 0)
        {
            StartCoroutine(ShowPopupThenDoCoroutine(message, d, x, y));
        }
        private IEnumerator ShowPopupThenDoCoroutine(string message, MyDelegate d, float x, float y)
        {
            //show the popup
            ShowPopup(message, x, y);
            //keep track of the popup we added
            var thePopup = popupMenus.ElementAt(0);
            //wait until the popup we just added is closed
            while (popupMenus.Contains(thePopup)) yield return new WaitForEndOfFrame();
            //execute the function
            d();
        }
    }
}





