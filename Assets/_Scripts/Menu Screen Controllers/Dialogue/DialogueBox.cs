﻿using UnityEngine;
using System.Collections;
using GSC;
using UnityEngine.UI;
using System;

public class DialogueBox : MonoBehaviour {
    GameSceneController gsc;
    Vector3 nextItemCoords;
    public GameObject buttonObj;

    // Use this for initialization
    void Awake () {
        gsc = GameObject.Find("GameSceneController").GetComponent<GameSceneController>();
        nextItemCoords = new Vector3(0, 0, 0);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    
    /// <summary>
    /// Creates a new button on this menu with text, buttonText, that calls onClick when pressed.
    /// </summary>
    /// <param name="buttonText"></param>
    /// <param name="onClick"></param>
    public void AddButton(string buttonText, MyDelegate onClick)
    {
        //use a coroutine to prevent the mouse selecting the added button as soon as it appears.
        StartCoroutine(AddButtonCoroutine(buttonText, onClick));
    }
    IEnumerator AddButtonCoroutine(string buttonText, MyDelegate onClick)
    {
        yield return null;
        //instantiate the button
        var bc = transform.Find("ButtonContainer").transform;
        var bObj = Instantiate(buttonObj, bc.transform.position, transform.rotation, bc.transform) as GameObject;
        var bTrans = bObj.GetComponent<RectTransform>();
        //set the position  
        bTrans.anchoredPosition = nextItemCoords;
        //add the onclick function
        bObj.GetComponent<Button>().onClick.AddListener(() => {
            onClick();
        });
        //set the text
        bTrans.GetComponentInChildren<Text>().text = buttonText;
        //get the position of the next button
        nextItemCoords.y -= 1.4f * bTrans.sizeDelta.y - 20;
    }

    /// <summary>
    /// Destroys the current dialogue box and returns a dialogue box which will follow the current dialogue box
    /// </summary>
    public DialogueBox CreateDialogueBox(GameElement element, string dialogue)
    {
        Destroy(gameObject);
        return gsc.dialogueController.CreateDialogueBox(element, dialogue);
    }

    /// <summary>
    /// Destroys the current dialogue box and returns a simple dialogue box which will follow the current dialogue box
    /// </summary>
    public DialogueBox ShowDialogue(GameElement element, string dialogue)
    {
        Destroy(gameObject);
        return gsc.dialogueController.ShowDialogue(element, dialogue);
    }


    /// <summary>
    /// Destroys this dialogue box and reactivates the controls
    /// </summary>
    public void EndDialogue()
    {
        Destroy(gameObject);
        //return to the playing state
        gsc.curGameState = GameSceneController.GameState.PLAYING;
    }
}
