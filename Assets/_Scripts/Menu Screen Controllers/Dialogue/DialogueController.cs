﻿using UnityEngine;
using System.Collections;
using GSC;
using UnityEngine.UI;

public class DialogueController : GameElement
{
    public GameObject dialogueObj;
    public Font dialogueFont;



    new void Awake()
    {
        base.Awake();
        if (dialogueFont == null) Debug.LogError("Please set dialogueFont in MenuUtility");

    }

    public DialogueBox CreateDialogueBox(GameElement element, string dialogue)
    {
        //we are now in the dialogue game state
        gsc.curGameState = GameSceneController.GameState.IN_DIALOGUE;
        //disable and hide the buttons 
        //if (gsc.buttonSimulator != null)
        //{
        //    gsc.buttonSimulator.LockButtons();
        //}
        //show the dialogue
        var dObj = Instantiate(dialogueObj, Vector2.zero, Quaternion.identity) as GameObject;
        //set the properties
        var iconObj = dObj.transform.FindChild("Icon");
        if (iconObj != null)
            iconObj.GetComponent<Image>().sprite = element.elementIcon;
        var nameObj = dObj.transform.FindChild("Name");
        if(nameObj != null) 
            nameObj.GetComponent<Text>().text = element.elementName;
        var descriptionObj = dObj.transform.FindChild("Description");
        if(descriptionObj != null) 
            descriptionObj.GetComponent<Text>().text = element.elementDescription;
        dObj.transform.FindChild("ScrollableTextObj").FindChild("Image").FindChild("Text").GetComponent<Text>().text = dialogue;
        //set the parent to be the canvas and set the order
        dObj.transform.SetParent(gsc.canvas.transform);
        dObj.transform.SetSiblingIndex(gsc.transition.transform.GetSiblingIndex());
        //set the position
        var dt = dObj.GetComponent<RectTransform>();
        dt.anchoredPosition = Vector2.zero;
        dt.sizeDelta = new Vector2(0, 0);
        //return the dialogue box
        return dObj.GetComponent<DialogueBox>();
    }
    public DialogueBox ShowDialogue(GameElement element, string dialogue)
    {
        var dBox = CreateDialogueBox(element, dialogue);
        dBox.AddButton("OK", delegate ()
        {
            dBox.EndDialogue();
        });
        //StartCoroutine(DestroyDialogueBoxOnKey(dBox, KeyCode.C));
        return dBox;
    }
    //IEnumerator DestroyDialogueBoxOnKey(DialogueBox d, KeyCode k)
    //{
    //    yield return new WaitForEndOfFrame();
    //    while (!Input.GetKeyDown(k))
    //    {
    //        yield return null;
    //        if (d == null) break;
    //    }
    //    if(d != null) d.EndDialogue();
    //}

    /*
    public DialogueBox CreateDialogueBox(Item item, string dialogue)
    {
        var g = new GameElement();
        g.elementName = item.itemName;
        g.elementDescription = item.itemDescription;
        g.elementIcon = item.itemIcon;
        var dBox = CreateDialogueBox(g, dialogue);
        return dBox;
    }
    public DialogueBox ShowDialogue(Item item, string dialogue)
    {
        var g = new GameElement();
        g.elementName = item.itemName;
        g.elementDescription = item.itemDescription;
        g.elementIcon = item.itemIcon;
        var dBox = ShowDialogue(g, dialogue);
        return dBox;
    }
    */
}
