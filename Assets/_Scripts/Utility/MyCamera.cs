﻿using UnityEngine;
using System.Collections;
using System;

namespace GSC
{
    [RequireComponent (typeof (Camera))]
    public class MyCamera : MonoBehaviour
    {
        GameSceneController gsc;
        Vector3 oldPlayerPos;
        void OnEnable()
        {
            oldPlayerPos = new Vector3(0, 0, 0);
            gsc = GameObject.Find("GameSceneController").GetComponent<GameSceneController>();
            var a = GetComponent<Animator>();
            if(a != null) a.enabled = false;
        }

        void Update()
        {
            if (gsc.player == null) return;
            //big centerbox used to test interactive objects' dynamic hover effects
            //var centerBox = new Vector2(15, 14);
            var centerBox = new Vector2(10, 4);
            MoveWithPlayerBox(centerBox.x/2.0f, centerBox.x/2.0f, centerBox.y / 2.0f, centerBox.y / 2.0f);
        }


        public void ResetCameraPos()
        {
            var p = gsc.player.transform.position;
            var newPos = new Vector3(p.x, p.y, transform.position.z);
            transform.position = newPos;
        }


        public void Shake(float duration, float magnitude)
        {   StartCoroutine(ShakeCoroutine(duration, magnitude));    }
        private IEnumerator ShakeCoroutine(float duration, float magnitude)
        {

            float elapsed = 0.0f;

            Vector3 originalCamPos = Camera.main.transform.position;
            var player = gsc.player;

            while (elapsed < duration)
            {

                elapsed += Time.deltaTime;

                float percentComplete = elapsed / duration;
                float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

                // map value to [-1, 1]
                float x = UnityEngine.Random.value * 2.0f - 1.0f;
                float y = UnityEngine.Random.value * 2.0f - 1.0f;
                x *= magnitude * damper;
                y *= magnitude * damper;

                transform.position = player.transform.position + new Vector3(x, y, originalCamPos.z);

                yield return null;
            }

            Camera.main.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, transform.position.z);
        }


        /// <summary>
        /// Follows the player directly
        /// </summary>
        private void MoveWithPlayer()
        {
            var pPos = gsc.player.transform.position;
            this.transform.position = new Vector3(pPos.x, pPos.y, transform.position.z);
        }


        /// <summary>
        /// Follows the player when the player leaves the bounding box described by the parameters
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <param name="up"></param>
        /// <param name="down"></param>
        private void MoveWithPlayerBox(float leftDistance, float rightDistance, float topDistance, float bottomDistance)
        {
            var pPosDiff = gsc.player.transform.position - oldPlayerPos;
            var newPos = transform.position;
            if (gsc.player.transform.position.x > transform.position.x + rightDistance)
            {
                newPos.x += pPosDiff.x;
            }
            else if (gsc.player.transform.position.x < transform.position.x - leftDistance)
            {
                newPos.x -= -pPosDiff.x;
            }

            if (gsc.player.transform.position.y > transform.position.y + topDistance)
            {
                newPos.y += pPosDiff.y;
            }
            else if (gsc.player.transform.position.y < transform.position.y - bottomDistance)
            {
                newPos.y -= -pPosDiff.y;
            }
            transform.position = newPos;

            oldPlayerPos = gsc.player.transform.position;
        }
    }
}
