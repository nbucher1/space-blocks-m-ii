﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class ParticleSystemDestroyer : MonoBehaviour {

	// Use this for initialization
	IEnumerator Start () {
        //get all the particle systems
        var pSystems = GetComponentsInChildren<ParticleSystem>();
        //get the maximum duration of the particle systems
        var maxDuration = 0.0f;
        foreach(var s in pSystems)
        {
            if(s.duration > maxDuration)
            {
                maxDuration = s.duration;
            }
        }
        //wait for the max duration
        yield return new WaitForSeconds(maxDuration);
        //destroy this object
        Destroy(this.gameObject);
	}
}
