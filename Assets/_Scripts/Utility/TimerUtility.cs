﻿using UnityEngine;
using System.Collections;

namespace GSC
{
    public class TimerUtility : MonoBehaviour
    {
        public float buttonLockTime = .0001f;

        void FixedUpdate()
        {
            if(DynamicInput.IsPressed_P1(ButtonType.A)) PreventDebounceButtonA();
        }
        /// <summary>
        /// Waits waitTime, then performs MyDelegate d. MyDelgate has the signature, 'public void MyDelegate();'
        /// </summary>
        /// <param name="waitTime"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        private IEnumerator WaitThenDoCoroutine(float waitTime, MyDelegate d)
        {
            yield return new WaitForSeconds(waitTime);
            d();
        }
        public void toggleSpeed()
        {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 3;
            }
            else
            {
                Time.timeScale = 1;
            }
        }

        /// <summary>
        /// Waits waitTime, then performs MyDelegate d. MyDelgate has the signature, 'public void MyDelegate();'
        /// </summary>
        /// <param name="waitTime"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public void WaitThenDo(float waitTime, MyDelegate d)
        {
            StartCoroutine(WaitThenDoCoroutine(waitTime, d));
        }


        public void LockButtonA(float time)
        {
            StartCoroutine(LockButtonACoroutine(time));
        }
        private IEnumerator LockButtonACoroutine(float time)
        {
            yield return new WaitForEndOfFrame();
            DynamicInput.buttonALocked = true;
            yield return new WaitForSeconds(time);
            DynamicInput.buttonALocked = false;
        }
        public void PreventDebounceButtonA()
        {
            LockButtonA(buttonLockTime);
        }



        public void LockControls()
        {
            DynamicInput.controlsLocked = true;
        }
        public void UnlockControls()
        {
            DynamicInput.controlsLocked = false;
        }
        public void LockControlsFor(float seconds)
        {
            StartCoroutine(LockControllerForCoroutine(seconds));
        }
        /// <summary>
        /// Locks the controller for the parameter, seconds, seconds
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        private IEnumerator LockControllerForCoroutine(float seconds)
        {
            LockControls();
            yield return new WaitForSeconds(seconds);
            UnlockControls();
        }
    }
}
