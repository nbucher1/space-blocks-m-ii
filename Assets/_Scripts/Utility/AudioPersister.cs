﻿using UnityEngine;
using System.Collections;

public class AudioPersister : MonoBehaviour {
    private static AudioPersister instance = null;
    public static AudioPersister Instance
    {
        get { return instance; }
    }
    void Awake()
    {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
            return;
        } else {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
