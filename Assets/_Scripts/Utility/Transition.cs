﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace GSC
{
    public class Transition : MonoBehaviour 
    {
        float transitionTime = 0.0f;
        GameSceneController gsc;
        public bool isDimmed = false;
        
        void Awake()
        {
            gsc = GameObject.Find("GameSceneController").GetComponent<GameSceneController>();
        }



        /// <summary>
        /// locks the player's controls, fades out the screen, sets the next level, and waits then calls LoadNextLevel()
        /// </summary>
        /// <param name="level"></param>
	    public void LoadLevel(string level)
        {
            transitionTime = Time.time + 1.0f;  //the time the scene will transition to the next one
            CheckAudio(level);
            //save the name of this scene in the LastSceneLoaded PlayerPref
            PlayerPrefs.SetString("LastSceneLoaded", SceneManager.GetActiveScene().name);
            //save all the element's preferences
            var elements = GameObject.FindObjectsOfType<GameElement>();
            foreach(var e in elements)
            {
                e.SavePrefs();
            }
            Time.timeScale = 1.0f;
            gsc.timerUtility.LockControls(); //lock player's controls   
            GetComponent<Animator>().Play("TransitionFadeIn");
            gsc.timerUtility.WaitThenDo(1, delegate ()
            {
                SceneManager.LoadScene(level);
            });
        }

        void CheckAudio(string nextLevel)
        {
            if (gsc.audioController == null) return;
            var curLevel = SceneManager.GetActiveScene().name;
            if (//isChasingCaptain ||
                //(
                (curLevel == "1-LeftWing" ||
                curLevel == "2-CenterArea" ||
                curLevel == "3-RightWing") 
                &&
                (nextLevel == "1-LeftWing" ||
                nextLevel == "2-CenterArea" ||
                nextLevel == "3-RightWing"))
                //)
            {
                if(gsc.audioController.GetComponent<AudioPersister>() == null)
                {
                    //make sure the audio persists in these scene transitions
                    gsc.audioController.gameObject.AddComponent<AudioPersister>();

                }
            }
            else //if(gsc.soundPlayer.GetComponent<AudioPersister>() != null)
            {
                //remove the audo persister otherwise
                //Destroy(gsc.soundPlayer.gameObject);
                gsc.audioController.RolloffSound(.05f);
            }
        }


        public void DimScreen()
        {
            GetComponent<Animator>().Play("TransitionDim");
            isDimmed = true;
        }
        public void UndimScreen()
        {
            GetComponent<Animator>().Play("TransitionHidden");
            isDimmed = false;
        }
    }
}