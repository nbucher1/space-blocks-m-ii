﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonColorChanger : MonoBehaviour
{
    public void OnMouseEnter()
    {
        transform.GetComponentInChildren<Text>().color = new Color(
            0.83529411764f,
            0.81176470588f,
            0.09411764705f);
    }
    public void OnMouseExit()
    {
        transform.GetComponentInChildren<Text>().color = new Color(
            0.0784313725f,
            0.65098039215f,
            0.96862745098f);
    }
}
