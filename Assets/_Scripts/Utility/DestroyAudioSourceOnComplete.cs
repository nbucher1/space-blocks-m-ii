﻿using UnityEngine;
using System.Collections;

public class DestroyAudioSourceOnComplete : MonoBehaviour {

    AudioSource a;
	// Use this for initialization
	void Awake () {
        a = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(!a.isPlaying)
        {
            Destroy(this.gameObject);
        }
	}

}
