﻿using UnityEngine;
using System.Collections;
using GSC;

public class UIResize : MonoBehaviour {
    public float heightScale;
    public float widthScale;
    GameSceneController gsc;

	// Use this for initialization
	void Start () {
        //Debug.Log("screen.height " + Screen.height);
        //Debug.Log("screen.currentres " + Screen.currentResolution);
        //Debug.Log("screen.dpi " + Screen.dpi);
        gsc = GameObject.Find("GameSceneController").GetComponent<GameSceneController>();

        var trans = GetComponent<RectTransform>();

        //adjusts the scale to match the screen that it's being played on
        //heightScale = gsc.UIResizeScale.y;//(Screen.height / 270.0f);//180.0f);
        //widthScale = gsc.UIResizeScale.x;//(Screen.width / 480.0f);// 320.0f);
        //trans.localScale = new Vector3(widthScale, heightScale, 1);

        //adjusts the x and y positions to match the scale of the screen that it's being played on
        var adjustedY = trans.anchoredPosition.y * heightScale;
        var adjustedX = trans.anchoredPosition.x * widthScale;
        trans.anchoredPosition = new Vector2(adjustedX, adjustedY);
    }
}
