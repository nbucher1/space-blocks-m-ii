﻿using UnityEngine;
using System.Collections;

namespace GSC
{
    public enum ButtonType
    {
        A,
        B,
        X,
        Y,
        START,
        SELECT,
        L1,
        R1,
        L2,
        R2,
        UP,
        DOWN,
        LEFT,
        RIGHT,
        D_UP,
        D_DOWN,
        D_LEFT,
        D_RIGHT,
        C_UP,
        C_DOWN,
        C_LEFT,
        C_RIGHT,
        R3,
        L3
    }
}
