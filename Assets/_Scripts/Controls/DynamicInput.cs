﻿using UnityEngine;
using System.Collections;

namespace GSC
{
    public static class DynamicInput 
    {
        public static bool horizontalLocked = false;
        public static bool verticalLocked = false;
        public static float deadArea = .7f;
        public static bool buttonALocked = false;
        public static bool controlsLocked = false;
        public static bool buttonAPressed = false;
        public static bool buttonBDown;

        public static bool GetButtonP1(ButtonType button)
        { 
            if(button == ButtonType.B && buttonBDown) return true;

            return false;
        }


        public static bool IsPressed_P1(ButtonType button)
        {
            if (controlsLocked) return false;   //if the controls are locked, don't register input
            //Debug.Log(Input.GetAxis("Horizontal") + "\thorizlocked: " + horizontalLocked + "\tvertlocked: " + verticalLocked);
            if (Mathf.Abs(Input.GetAxis("Horizontal")) < deadArea) horizontalLocked = false;
            if (Mathf.Abs(Input.GetAxis("Vertical")) < deadArea) verticalLocked = false;
            switch(button)
            {
                case ButtonType.A:
                    if ((Input.GetKeyDown(KeyCode.Joystick1Button0) ||
                        //Input.GetMouseButtonDown(0) ||
                        Input.GetKeyDown("e") || 
                        buttonAPressed) && !buttonALocked) return true;
                    break;
                case ButtonType.B:
                    if (Input.GetKeyDown(KeyCode.Joystick1Button1) || 
                        Input.GetKeyDown("x")) return true;
                    break;
                case ButtonType.X:
                    if (Input.GetKeyDown(KeyCode.Joystick1Button2) || 
                        Input.GetKeyDown("s")) return true;
                    break;
                case ButtonType.Y:
                    if (Input.GetKeyDown(KeyCode.Joystick1Button3) || 
                        Input.GetKeyDown("d")) return true;
                    break;
                case ButtonType.START:
                    if (Input.GetKeyDown(KeyCode.Joystick1Button7) || 
                        Input.GetKeyDown(KeyCode.Return)) return true;
                    break;
                case ButtonType.SELECT:
                    break;
                case ButtonType.L1:
                    break;
                case ButtonType.R1:
                    break;
                case ButtonType.L2:
                    break;
                case ButtonType.R2:
                    break;
                case ButtonType.UP:
                    if ((!verticalLocked && Input.GetAxis("Vertical") > .8f) ||
                        Input.GetKeyDown("up"))
                    {
                        horizontalLocked = false;
                        verticalLocked = true;
                        return true;
                    }
                    break;
                case ButtonType.DOWN:
                    if ((!verticalLocked && Input.GetAxis("Vertical") < -.8f) ||
                        Input.GetKeyDown("down"))
                    {
                        horizontalLocked = false;
                        verticalLocked = true;
                        return true;
                    }
                    break;
                case ButtonType.LEFT:
                    if ((!horizontalLocked && Input.GetAxis("Horizontal") < -.8f) ||
                        Input.GetKeyDown("left"))
                    {
                        horizontalLocked = true;
                        verticalLocked = false;
                        return true;
                    }
                    break;
                case ButtonType.RIGHT:
                    if ((!horizontalLocked && Input.GetAxis("Horizontal") > .8f) ||
                        Input.GetKeyDown("right"))
                    {
                        horizontalLocked = true;
                        verticalLocked = false;
                        return true;
                    }
                    break;
                case ButtonType.D_UP:
                    if (Input.GetAxis("D_Vertical_P1") > 0 ||
                        Input.GetKeyDown("right")) return true;
                    break;
                case ButtonType.D_DOWN:
                    if (Input.GetAxis("D_Vertical_P1") < 0 ||
                        Input.GetKeyDown("right")) return true;
                    break;
                case ButtonType.D_LEFT:
                    if (Input.GetAxis("D_Horizontal_P1") < 0 ||
                        Input.GetKeyDown("right")) return true;
                    break;
                case ButtonType.D_RIGHT:
                    if (Input.GetAxis("D_Horizontal_P1") > 0 ||
                        Input.GetKeyDown("right")) return true;
                    break;
                case ButtonType.C_UP:
                    break;
                case ButtonType.C_DOWN:
                    break;
                case ButtonType.C_LEFT:
                    break;
                case ButtonType.C_RIGHT:
                    break;
                case ButtonType.R3:
                    break;
                case ButtonType.L3:
                    break;
                default:
                    Debug.Log("Button type not found in DynamicInput.cs");
                    break;
            }
            return false;
        }

    }
}
