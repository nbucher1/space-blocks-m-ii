﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GSC;
using System.Linq;

public class BasicTurret : Turret
{
    
    protected override void Awake()
    {
        base.Awake();
        /*
        //should be specified in editor
        cost = 20;
        cooldown = 2.0f;
        maxHealth = 20;
        attackDamage = 10;
        curHealth = maxHealth;
        */
    }

    /*
    // Update is called once per frame
    void Update()
    {
        var e = GetClosestGalagmanautInRange();
        if (e != null)
        {
            Debug.Log("aiming at closest enemy");
            StartCoroutine(AttackCoroutine(e));
        }
    }

    IEnumerator AttackCoroutine(Transform e)
    {
        if (canAttack)
        {
            canAttack = false;
            Destroy(e);
            yield return new WaitForSeconds(cooldown);
            canAttack = true;
        }
    }
    //*/
}
