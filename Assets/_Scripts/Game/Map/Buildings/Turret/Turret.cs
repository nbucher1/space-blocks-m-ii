﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GSC;
using System.Linq;

public class Turret : Building
{
    public GameObject barrelObj;
    bool canAttack = true;
    Transform closestGalagmanaut = null;
    Transform closestGalagmanautInRange = null;

    protected override void Awake()
    {
        base.Awake();
        /*
        //should be specified in editor
        cost = 20;
        cooldown = 2.0f;
        maxHealth = 20;
        attackDamage = 10;
        curHealth = maxHealth;
        */
        StartCoroutine(UpdateClosestGalagmanautCoroutine());
        StartCoroutine(AimAtClosestGalagmanautCoroutine());
    }

    // Update is called once per frame
    void Update()
    {
        //if paused or in dialogue, return
        if (gsc.curGameState == GameSceneController.GameState.PAUSED ||
            gsc.curGameState == GameSceneController.GameState.IN_DIALOGUE) return;
        if (closestGalagmanautInRange != null && canAttack)
        {
            Attack(closestGalagmanautInRange.GetComponent<Galagmanaut>());
            StartCoroutine(CooldownCoroutine());
        }
    }



    //------------------[ ATTACKING ]-----------------------//

    void Attack(Galagmanaut g)
    {
        g.TakeDamage(attackDamage);
        //if the galagmanaut was destroyed, update the galagmanaut references
        if (g != null || g.curHealth <= 0)
        {
            UpdateClosestGalagmanauts();
        }
    }

    IEnumerator CooldownCoroutine()
    {
        canAttack = false;
        yield return new WaitForSeconds(cooldown);
        canAttack = true;
    }

    //------------------[ AIMING AND UPDATING CLOSEST GALAGMANAUTS ]-----------------------//
    IEnumerator AimAtClosestGalagmanautCoroutine()
    {
        while (true)
        {
            AimAtClosestGalagmanaut();
            yield return new WaitForSeconds(.05f);
        }
    }
    void AimAtClosestGalagmanaut()
    {
        if (closestGalagmanaut != null)
        {
            //point at closest galagmanaut
            var dir = closestGalagmanaut.position - barrelObj.transform.position;
            barrelObj.transform.up = dir.normalized;
        }
        else
        {
            barrelObj.transform.localRotation = this.transform.localRotation;
        }
    }

    IEnumerator UpdateClosestGalagmanautCoroutine()
    {
        while (true)
        {
            UpdateClosestGalagmanauts();
            if (closestGalagmanaut == null)
            {
                //wait a bit more to make sure a galagmanaut's there
                yield return new WaitForSeconds(.5f);
            }
            yield return new WaitForSeconds(.5f);
        }
    }
    private void UpdateClosestGalagmanauts()
    {
        //get the closest galagmanaut in range (null if it doesn't exist)
        closestGalagmanautInRange = GetClosestGalagmanautInRange();
        //get the closest galagmanaut regardless of range
        if (closestGalagmanautInRange == null)
        {
            closestGalagmanaut = GetClosestGalagmanaut();
        }
        else
        {
            closestGalagmanaut = closestGalagmanautInRange;
        }
    }
}
