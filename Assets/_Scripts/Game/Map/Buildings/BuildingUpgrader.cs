﻿using UnityEngine;
using System.Collections;
using GSC;

public class BuildingUpgrader : GameElement
{
    Building buildingObj;

    protected override void Awake()
    {
        base.Awake();
        buildingObj = transform.parent.GetComponent<Building>();
    }

    public void OnMouseUp()
    {
        //if paused or in dialogue, return
        if (gsc.curGameState == GameSceneController.GameState.PAUSED ||
            gsc.curGameState == GameSceneController.GameState.IN_DIALOGUE) return;
        buildingObj.Upgrade();
    }
}
