﻿using UnityEngine;
using System.Collections;
using System;

public class Rocket : Projectile
{
    protected override void MoveUpdate()
    {
        //accelerate, but not past the topspeed
        if(curSpeed < topSpeed)
        {
            curSpeed = Mathf.Clamp(curSpeed + acceleration, 0, topSpeed);
        }
        //move in the target direction
        var movement = targetDirection * curSpeed * Time.deltaTime;
        this.transform.position += new Vector3(movement.x, movement.y, 0);
    }
}
