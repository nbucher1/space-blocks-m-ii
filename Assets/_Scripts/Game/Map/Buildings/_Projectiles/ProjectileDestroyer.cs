﻿using UnityEngine;
using System.Collections;

public class ProjectileDestroyer : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D coll)
    {
        var p = coll.GetComponent<Projectile>();
        if(p != null)
        {
            Destroy(p.gameObject);
        }
    }
}
