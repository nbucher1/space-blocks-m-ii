﻿using UnityEngine;
using System.Collections;
using GSC;
using System;
using System.Collections.Generic;
using System.Linq;

public class SpaceDebris : Projectile
{

    protected override void MoveUpdate()
    {
        //move in the target direction
        var movement = targetDirection * curSpeed * Time.deltaTime;
        this.transform.position += new Vector3(movement.x, movement.y, 0);
    }
}
