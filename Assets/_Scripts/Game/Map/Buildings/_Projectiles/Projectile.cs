﻿using UnityEngine;
using System.Collections;
using GSC;
using System;
using System.Collections.Generic;
using System.Linq;

public abstract class Projectile : GameElement
{
    public float curSpeed = 2;
    public float acceleration = 1.0f;
    public float topSpeed = 2;
    public int attackDamage = 5;
    protected Vector2 targetDirection;
    protected Transform targetObj;
    public GameObject explosionPrefab;

    float projectileTime = 0;


    public virtual void Initialize(float curSpeed, int attackDamage, Vector2 targetDirection, 
        Transform targetObj = null, float acceleration = 1.0f, float topSpeed = 1.0f)
    {
        this.curSpeed = curSpeed;
        this.acceleration = acceleration;
        this.topSpeed = topSpeed;
        this.attackDamage = attackDamage;
        this.targetDirection = targetDirection;
        this.targetObj = targetObj;
    }

    void Update()
    {
        //if paused or in dialogue, return
        if (gsc.curGameState == GameSceneController.GameState.PAUSED || 
            gsc.curGameState == GameSceneController.GameState.IN_DIALOGUE) return;
        //otherwise, keep going
        projectileTime += Time.deltaTime;
        //move in the target direction
        MoveUpdate();
    }

    protected abstract void MoveUpdate();

    void OnTriggerEnter2D(Collider2D coll)
    {
        var obj = coll.GetComponent<Galagmanaut>();
        if (obj != null)
        {
            //if we entered a galagmanaut
            obj.TakeDamage(attackDamage);
            //Instantiate(explosionPrefab, transform.position, Quaternion.identity, transform.parent);
            Destroy(this.gameObject);
        }
    }
}
