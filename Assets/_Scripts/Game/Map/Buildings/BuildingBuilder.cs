﻿using UnityEngine;
using System.Collections;
using GSC;
using System;
using System.Linq;

public class BuildingBuilder : GameElement
{
    bool isMouseOver = false;
    public GameObject selectionObj;
    bool isAimingBuilding = false;
    bool isPlacingBuilding = false;
    public GameObject constructionMenu;
    GameObject selectedBuildingPrefab;  //the reference to the type of building to be built
    GameObject activeBuilding;  //the reference to the building gameobject currently being built
    public GameObject[] buildingPrefabs;

    protected override void Awake()
    {
        base.Awake();
        selectedBuildingPrefab = null;
    }

    public void SelectBuilding<T>() where T : Building
    {
        selectedBuildingPrefab = buildingPrefabs.Where(b => b.GetComponent<T>() != null).SingleOrDefault();
        isPlacingBuilding = true;
    }

    public void SelectBuilding(string buildingType)
    {
        selectedBuildingPrefab = buildingPrefabs.Where(b => b.GetComponent(buildingType) != null).SingleOrDefault();
        if (selectedBuildingPrefab == null) Debug.LogError("Error when selecting building");
        else isPlacingBuilding = true;
    }
    public void DeselectBuilding()
    {
        selectedBuildingPrefab = null;
        isPlacingBuilding = false;
        isAimingBuilding = false;
    }



    public void OnMouseEnter()
    {
        isMouseOver = true;
    }
    public void OnMouseExit()
    {
        isMouseOver = false;
    }
    void OnMouseUpAsButton()
    {
        if (isPlacingBuilding)
        {
            //Debug.Log("mouse pressed at: " + Input.mousePosition);
            //get the mouse position in world space
            var mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mPos.z = selectedBuildingPrefab.transform.position.z;
            activeBuilding = Instantiate(selectedBuildingPrefab, mPos, Quaternion.identity, transform.parent) as GameObject;
            //stop placing, and start aiming this building
            isPlacingBuilding = false;
            isAimingBuilding = true;
        }
        else if (isAimingBuilding)
        {

        }
    }

    void Update()
    {
        if (isAimingBuilding && Input.GetMouseButtonDown(0))
        {
            //Debug.Log("mouse pressed at: " + Input.mousePosition);
            //get the mouse position in world space
            var mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            //point at the mouse position
            var rotation = mPos - activeBuilding.transform.position;
            //Debug.Log(rotation);
            rotation = new Vector3(rotation.x, rotation.y).normalized;
            //Debug.Log(rotation);
            activeBuilding.transform.up = rotation;

            //end the building process
            DeselectBuilding();
            constructionMenu.SetActive(true);
            //gsc.gameStateController.Resume();
        }

        if ((isMouseOver && isPlacingBuilding))
        {
            //TODO: don't know why the selection obj is not being set to active when placing building but is still moving to the mouse input

            selectionObj.SetActive(true);
            //move the selection object to the mouse position
            var mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            selectionObj.transform.position = new Vector3(mPos.x, mPos.y, 0);
        }
        else if (isAimingBuilding)
        {
            //TODO: don't know why the selection obj is not being set to active when placing building but is still moving to the mouse input
            //TODO: make a linerenderer aim here

            selectionObj.SetActive(true);
            //move the selection object to the mouse position
            var mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            selectionObj.transform.position = new Vector3(mPos.x, mPos.y, 0);
            //rotate the building to face the mouse position
            var rotation = mPos - activeBuilding.transform.position;
            rotation = new Vector3(rotation.x, rotation.y).normalized;
            activeBuilding.transform.up = rotation;
        }
        else
        {
            //move the selection object out of the way
            selectionObj.SetActive(false);
        }
    }
}
