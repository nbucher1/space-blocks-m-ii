﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GSC;
using System.Linq;

public class SpaceSlingshot : Building
{

    public float projectileSpeed = 2;

    public GameObject projectilePrefab;
    bool canAttack = true;
    float spaceSlingshotTime = 0;
    
    protected override void Awake()
    {
        base.Awake();
        /*
        //should be specified in editor
        cost = 20;
        cooldown = 2.0f;
        maxHealth = 20;
        attackDamage = 10;
        curHealth = maxHealth;
        */
    }

    // Update is called once per frame
    void Update()
    {
        //if paused or in dialogue, return
        if (gsc.curGameState == GameSceneController.GameState.PAUSED ||
            gsc.curGameState == GameSceneController.GameState.IN_DIALOGUE) return;
        spaceSlingshotTime += Time.deltaTime;

        if (canAttack)
        {
            Attack();
            StartCoroutine(CooldownCoroutine());
        }   
    }

    void Attack()
    {
        //instantiate and initialize the projectile
        var p = (Instantiate(projectilePrefab, transform.position, transform.rotation, 
            transform.parent) as GameObject).GetComponent<Projectile>();
        p.Initialize(projectileSpeed, attackDamage, transform.up);
    }

    IEnumerator CooldownCoroutine()
    {
        canAttack = false;
        yield return new WaitForSeconds(cooldown);
        canAttack = true;
    }
}
