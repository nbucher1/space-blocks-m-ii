﻿using UnityEngine;
using System.Collections;
using GSC;
using System;
using System.Collections.Generic;
using System.Linq;

public abstract class Building : GameElement
{
    public int cost;
    public float cooldown = 2.0f;
    public int curHealth;
    public int maxHealth = 20;
    public int attackDamage = 5;
    public GameObject explosionPrefab;
    protected List<GameObject> galagmanautsInRange;

    public GameObject upgradeMenuObj;
    public GameObject nextBuildingUpgradePrefab;
    public int upgradeCost = 10;

    protected override void Awake()
    {
        base.Awake();
        curHealth = maxHealth;
        galagmanautsInRange = new List<GameObject>();
        if (!CanBePlaced())
        {
            //if the building can't be placed, revert changes and destroy this object
            //also display a warning to the player
        }
    }

    public void Upgrade()
    {
        if (nextBuildingUpgradePrefab == null)
        {
            Debug.Log("No more upgrades");
            return;
        }
        //REPLACE THIS PART WITH SHOWING THE UPGRADE MENU. THEN THIS CAN BE DONE FROM THERE-----------------------------
        if (gsc.player.GetMoney() > upgradeCost) gsc.player.RemoveMoney(upgradeCost);
        else
        {
            Debug.Log("------NO MONEY FOR UPGRADES------");
            return;
        }
        Debug.Log("UPGRADING");
        //create the upgrade
        Instantiate(nextBuildingUpgradePrefab, transform.position, transform.rotation, transform.parent);
        //may want to play an effect here

        //destroy this object
        Destroy(this.gameObject);
    }



    bool CanBePlaced()
    {
        //check if can be placed using an object pooled collider object check
        //throw new NotImplementedException();
        return false;
    }



    void OnTriggerEnter2D(Collider2D coll)
    {
        var obj = coll.GetComponent<Galagmanaut>();
        if (obj != null && !galagmanautsInRange.Contains(obj.gameObject))
        {
            galagmanautsInRange.Add(obj.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        var obj = coll.GetComponent<Galagmanaut>();
        if (obj != null && galagmanautsInRange.Contains(obj.gameObject))
        {
            galagmanautsInRange.Remove(obj.gameObject);
        }
    }

    public List<GameObject> GetAllGalagmanautsInRange() { return galagmanautsInRange; }
    public Transform GetClosestGalagmanautInRange()
    {
        if (galagmanautsInRange.Count == 0) return null;
        //else there are galagmanauts in range
        var minDist = float.MaxValue;
        Transform minDistEnemy = null;
        for (var i = 0; i < galagmanautsInRange.Count; i++)
        {
            if (galagmanautsInRange[i] == null) galagmanautsInRange.RemoveAt(i);
        }
        foreach (var enemy in galagmanautsInRange)
        {
            if (enemy == null) continue; // because they can be destroyed at any point
            var curDist = Vector3.Distance(transform.position, enemy.transform.position);
            if (curDist < minDist)
            {
                minDist = curDist;
                minDistEnemy = enemy.transform;
            }
        }
        //galagmanautsInRange.Clear();
        return minDistEnemy;
    }

    public Transform GetClosestGalagmanaut()
    {
        if (gsc == null || gsc.galagmanautSpawner == null) return null;
        //get the closest galagmanaut
        var galagmanauts = gsc.galagmanautSpawner.transform.GetComponentsInChildren<Transform>();
        //remove the galagmanaut spawner parent object from the array
        galagmanauts = galagmanauts.Where(g => g.GetComponent<GalagmanautSpawner>() == null).ToArray();
        if (galagmanauts.Length == 0) return null;
        var minDist = float.MaxValue;
        float curDist;
        Transform closestGalagmanaut = null;
        foreach (var e in galagmanauts)
        {
            curDist = Vector3.Distance(e.transform.position, this.transform.position);
            if (curDist < minDist)
            {
                minDist = curDist;
                closestGalagmanaut = e;
            }
        }
        return closestGalagmanaut;
    }



    protected void OnDestroy()
    {
        //Debug.Log("BUILDING " + GetInstanceID() + " DESTROYED");
        //throw new NotImplementedException();
    }
}
