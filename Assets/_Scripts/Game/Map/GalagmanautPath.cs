﻿using UnityEngine;
using System.Collections;

public class GalagmanautPath : MonoBehaviour {
    public Transform[] pathPoints;

	// Use this for initialization
	void Start () {
	    
	}
	
	void OnDrawGizmos () {
        //draw a line between all the points
        Gizmos.color = Color.red;
        for(var i=0; i< pathPoints.Length - 1; i++)
        {
            Gizmos.DrawLine(pathPoints[i].position, pathPoints[i + 1].position);
        }
    }
}
