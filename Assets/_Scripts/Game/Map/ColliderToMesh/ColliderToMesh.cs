﻿using System;
using UnityEngine;


[RequireComponent(typeof(PolygonCollider2D))]
[ExecuteInEditMode]
public class ColliderToMesh : MonoBehaviour
{
    public GameObject[] areaMeshObjects;
    void Start()
    {
        var pcs = gameObject.GetComponents<PolygonCollider2D>();
        for(var i=0; i<pcs.Length; i++) 
        {
            if (i >= areaMeshObjects.Length) Debug.Log("Not enough mesh objects to draw the collider of on " + name);
            RenderColliderAsMesh(pcs[i], areaMeshObjects[i]);
        }
    }

    private void RenderColliderAsMesh(PolygonCollider2D pc2A, GameObject meshObj)
    {
        //Render thing
        int pointCount = 0;
        pointCount = pc2A.GetTotalPointCount();
        MeshFilter mf = meshObj.GetComponent<MeshFilter>();
        Mesh mesh = new Mesh();
        Vector2[] points = pc2A.points;
        Vector3[] vertices = new Vector3[pointCount];
        Vector2[] uv = new Vector2[pointCount];
        for (int j = 0; j < pointCount; j++)
        {
            Vector2 actual = points[j];
            vertices[j] = new Vector3(actual.x, actual.y, 0);
            uv[j] = actual;
        }
        Triangulator tr = new Triangulator(points);
        int[] triangles = tr.Triangulate();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uv;
        mf.mesh = mesh;
        //Render thing
    }
}