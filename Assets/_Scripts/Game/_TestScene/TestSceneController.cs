﻿using UnityEngine;
using System.Collections;
using GSC;

public class TestSceneController : GameSceneController {

	// Use this for initialization
	void Start () {
        var g = new GameElement();
        g.elementName = "narrator";
        dialogueController.ShowDialogue(g, "Hello");
        galagmanautSpawner.Spawn<GalagmanautMinion>(1);
        galagmanautSpawner.Spawn<GalagmanautChief>(5);
        //galagmanautSpawner.Spawn<GalagmanautChief>(5);
        galagmanautSpawner.Spawn<GalagmanautHighCommander>(3);
        galagmanautSpawner.Spawn<GalagmanautMinion>(100);

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
