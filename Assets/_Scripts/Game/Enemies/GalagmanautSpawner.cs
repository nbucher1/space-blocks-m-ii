﻿using UnityEngine;
using System.Collections;
using GSC;
using System.Collections.Generic;
using System;

public class GalagmanautSpawner : GameElement {
    public GameObject galagmanautMinionPrefab;
    public GameObject galagmanautChiefPrefab;
    public GameObject galagmanautHighCommanderPrefab;
    [SerializeField]
    private float spawnWaitTime = 1.0f;

    private Queue<Type> spawnQueue;
    bool spawning = false;
    Vector3 initialSpawnPoint;

	// Use this for initialization
	protected override void Awake () {
        base.Awake();
        spawnQueue = new Queue<Type>();
	}
    
    public void Spawn<T>(int numToSpawn) where T : Galagmanaut
    {
        for(int i=0; i<numToSpawn; i++)
        {
            spawnQueue.Enqueue(typeof(T));
        }
        StartCoroutine(SpawnCoroutine(numToSpawn));
    }
    private IEnumerator SpawnCoroutine(int numToSpawn)
    {
        if(initialSpawnPoint == null) initialSpawnPoint = gsc.galagmanautPath.pathPoints[0].position;
        //wait for the previous spawn to finish
        while (spawning) yield return new WaitForEndOfFrame();
        //block other coroutines from spawning
        spawning = true;
        //spawn only the number of objects from the spawnQueue we are asked to spawn
        for (int i = 0; i < numToSpawn; i++)
        {
            //don't spawn during dialogue
            while (gsc.curGameState == GameSceneController.GameState.IN_DIALOGUE) yield return new WaitForEndOfFrame();

            var spawnType = spawnQueue.Dequeue();
            if (spawnType == typeof(GalagmanautMinion))
            {
                var g = Instantiate(galagmanautMinionPrefab, initialSpawnPoint, Quaternion.identity, this.transform) as GameObject;
            }
            else if (spawnType == typeof(GalagmanautChief))
            {
                var g = Instantiate(galagmanautChiefPrefab, initialSpawnPoint, Quaternion.identity, this.transform) as GameObject;
            }
            else if (spawnType == typeof(GalagmanautHighCommander))
            {
                var g = Instantiate(galagmanautHighCommanderPrefab, initialSpawnPoint, Quaternion.identity, this.transform) as GameObject;
            }
            else
            {
                Debug.Log("unsupported type to spawn");
            }
            //cool down before spawning another one
            yield return new WaitForSeconds(spawnWaitTime);
        }
        //allow other coroutines to spawn now
        spawning = false;
    }
}
