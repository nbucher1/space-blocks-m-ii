﻿using UnityEngine;
using System.Collections;
using GSC;

public class GalagmanautDestroyer : GameElement
{   
    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.GetComponent<Galagmanaut>() != null)
        {
            Destroy(coll.gameObject);
            Debug.Log("Damage Taken");  
        }
    }
}
