﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GSC
{
    public class Stats
    {
        public int health;
        public int physicalStrength;
        public int physicalDefense;
        public int magicStrength;
        public int magicDefense;
        //luck is out of 100
        //ex: 30 luck
        //means a 3 out of 10 chance to hit the target
        //and if we do hit the target
        //there is a (100-30) = 70 percent chance we will do
        //0% to 30% less damage than whichever strength value we are using to attack
        public int luck;



        public Stats()
        {
            this.health             = 0;
            this.physicalStrength   = 0;
            this.physicalDefense    = 0;
            this.magicStrength      = 0;
            this.magicDefense       = 0;
            this.luck               = 0;
        }



        public Stats(int health, int physicalStrength, int physicalDefense, int magicStrength, int magicDefense, int luck)
        {
            this.health             = health;
            this.physicalStrength   = physicalStrength;
            this.physicalDefense    = physicalDefense;
            this.magicStrength      = magicStrength;
            this.magicDefense       = magicDefense;
            this.luck               = luck;
        }



        
        public static Stats operator *(Stats x, int y)
        {
            Stats result = new Stats(
                x.health * y,
                x.physicalStrength * y,
                x.physicalDefense * y,
                x.magicStrength * y,
                x.magicDefense * y,
                x.luck * y);
            return result;
        }


        public static Stats operator +(Stats x, Stats y)
        {
            Stats result = new Stats(
                x.health + y.health,
                x.physicalStrength + y.physicalStrength,
                x.physicalDefense + y.physicalDefense,
                x.magicStrength + y.magicStrength,
                x.magicDefense + y.magicDefense,
                x.luck + y.luck);
            return result;
        }



        public static Stats operator -(Stats x, Stats y)
        {
            Stats result = new Stats(
                x.health            - y.health,
                x.physicalStrength  - y.physicalStrength,
                x.physicalDefense   - y.physicalDefense,
                x.magicStrength     - y.magicStrength,
                x.magicDefense      - y.magicDefense,
                x.luck              - y.luck);
            return result;
        }
    }
}
