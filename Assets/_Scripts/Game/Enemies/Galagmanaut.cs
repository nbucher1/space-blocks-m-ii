﻿using UnityEngine;
using System.Collections;
using GSC;
using System;
using UnityEngine.SceneManagement;

public abstract class Galagmanaut : GameElement
{
    //[Range(0, 5)]
    public int curHealth;
    public int maxHealth = 10;
    public float speed = 1.3f;
    public int attackDamage = 10;
    public int money = 1;
    public GameObject explodePrefab;


    Vector2 startPos;
    Vector2 targetPos;
    int curTarget = 1;  //start by targeting the first path point

    protected float galagmanautTime = 0.0f;

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
        curHealth = maxHealth;
        galagmanautTime = 0.0f;

        transform.position = gsc.galagmanautPath.pathPoints[0].position;
        startPos = transform.position;
        targetPos = gsc.galagmanautPath.pathPoints[curTarget].position;
    }

    internal void TakeDamage(int attackDamage)
    {
        curHealth -= attackDamage;
        //indicate that damage was taken
        StartCoroutine(IndicateDamageCoroutine(attackDamage));
        //check if dead
        if(curHealth < 0)
        {
            //if damage was taken and now the galagmanaut is dead, 
            //  add money to the player, explode, and destroy this gameobject
            gsc.player.AddMoney(this.money);
            Instantiate(explodePrefab, transform.position, Quaternion.identity, transform.parent);
            Destroy(this.gameObject);
        }
    }
    IEnumerator IndicateDamageCoroutine(int damageAmount)
    {
        var colorChange = damageAmount / 30.0f;
        var csr = GetComponentsInChildren<SpriteRenderer>();
        foreach (var sr in csr)
        {
            sr.color = new Color(sr.color.r, sr.color.g - colorChange, sr.color.b - colorChange);
        }
        yield return new WaitForSeconds(.2f);
        foreach (var sr in csr)
        {
            sr.color = new Color(sr.color.r, sr.color.g + colorChange, sr.color.b + colorChange);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gsc.curGameState == GameSceneController.GameState.IN_DIALOGUE) return;

        //multiply by speed to make speed a non-factor in the movement direction
        galagmanautTime += Time.deltaTime * speed;

        //call the galagmanaut-specific move function
        Move();
    }
    protected virtual void Move()
    {
        //move at a constant speed along the path
        var moveDisplacement = GetMoveDisplacement(transform.position, Time.deltaTime);
        transform.position = transform.position + new Vector3(moveDisplacement.x, moveDisplacement.y, 0);
    }
    protected Vector2 GetMoveDisplacement(Vector2 curPos, float timeInterval)
    {
        var directionToTarget = (targetPos - curPos).normalized;
        var distanceToTarget = Vector2.Distance(curPos, targetPos);
        // if the target will take more than one move to get to
        if (distanceToTarget > speed * timeInterval)
        {
            return directionToTarget * speed * timeInterval;
        }
        // else, don't overshoot the target
        else
        {
            // set the current position to the target's position
            curPos = targetPos;
            // get the time left after reaching the current target
            var timeLeft = timeInterval - distanceToTarget / speed;
            // set the next target to the current target
            AimAtNextPathTarget();
            // calculate the displacement from the current target to the next target using the time left
            return GetMoveDisplacement(curPos, timeLeft);
        }
    }
    protected void AimAtNextPathTarget()
    {
        curTarget++;
        if (curTarget >= gsc.galagmanautPath.pathPoints.Length)
        {
            Debug.Log("at end of path");
            return;
        }
        targetPos = gsc.galagmanautPath.pathPoints[curTarget].position;
    }


    public void GetBuildingsInRange()
    {
        throw new NotImplementedException();
    }
    public void IsInRange(Building b)
    {
        throw new NotImplementedException();
    }
    public void AttackSettlement()
    {
        Debug.Log("SETTLEMENT ATTACKED BY " + this.GetInstanceID());
        gsc.player.TakeDamage(attackDamage);
    }
    protected void OnDestroy()
    {
        if (applicationClosing) return;
        //Debug.Log("GALAGMANAUT " + this.GetInstanceID() + " DESTROYED");
    }
}
