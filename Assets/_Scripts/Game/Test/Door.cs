﻿using UnityEngine;
using System.Collections;
using System;

public class Door : MonoBehaviour, IInteractive {
    Animator a;

	// Use this for initialization
	void Start () {
        a = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Interact()
    {
        a.SetBool("open", !a.GetBool("open"));
        Debug.Log("interacting");
    }

    public void Select()
    {
        Debug.Log(name + " Selected");
    }

    public void Deselect()
    {
        Debug.Log(name + " Deselected");
    }
}
