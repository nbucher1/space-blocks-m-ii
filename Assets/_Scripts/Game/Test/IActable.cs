﻿using UnityEngine;
using System.Collections;

public interface IInteractive {
    void Select();
    void Deselect();
    void Interact();
}
