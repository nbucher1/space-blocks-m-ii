﻿using UnityEngine;
using System.Collections;
using GSC;

public class Box : GameElement, IInteractive {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

    }
    public void Interact()
    {
        var d = gsc.dialogueController.CreateDialogueBox(this, "These are not the droids you're looking for.");
        d.AddButton("What?!? Who said that?", delegate ()
        {
            var d2 = d.CreateDialogueBox(this, "");
            d2.AddButton("... < you look around, but don't see anyone >", delegate() { d2.EndDialogue(); });
        });
        Debug.Log("interacting");
    }

    public void Select()
    {
        Debug.Log(name + " Selected");
    }

    public void Deselect()
    {
        Debug.Log(name + " Deselected");
    }
}
