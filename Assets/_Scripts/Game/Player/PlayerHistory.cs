﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GSC
{
    public class PlayerHistory
    {


        #region WanderingBobQuest
        public bool talkedToBobLeftWing = false;
        public bool talkedToBobRightWing = false;
        public bool talkedToBobCenter = false;
        public bool talkedToBobStarDeck = false;
        public bool talkedToBobEngineRoom = false;
        #endregion

        #region ToddMiniGameQuest
        public bool hasTalkedToTodd = false;
        public bool hasPlayedMiniGame = false;
        public bool hasWonMiniGame = false;
        #endregion

        #region Woodrow'sRealNamePasswordClue
        public bool learnedWoodrowsRealName = false;
        public bool impressedWoodrowWithRealName = false;
        public bool gotAccessToToyDroid = false;
        #endregion

        #region littleTommyPasswordClue
        public bool gotLetterFromLittleTommy = false;
        #endregion

        #region littleAmyPasswordQuest
        public bool gotQuestfromLittleAmy = false;
        public bool gotLittleBubsIDCard = false;
        #endregion

        #region ClaireParticleComputerQuest
        public bool computerFixed = false;
        public bool talkedToClaire = false;
        public bool gotParticleDriveFromTyrone = false;
        #endregion

        #region Tony
        public bool askedTonyAboutEarth = false;
        public bool askedTonyAboutHimself = false;
        public bool askedTonyAboutTheCaptain = false;
        public bool askedTonyAboutTheBubs = false;
        public bool finishedTonysDialogue = false;
        #endregion



        #region OldRingQuest
        public bool talkedToSteve = false;
        public bool talkedToTommy = false;
        public bool oldRingQuestComplete = false;
        #endregion


        #region LisaJournal
        public bool hasReadLisaFirstJournalEntry = false;
        public bool talkedToOlas = false;
        public bool hasSeenScandal = false;
        #endregion

        #region AliceAndPhillis
        public bool hasHeardOfAlicesStoryFromPhillis = false;
        public bool toldAliceAboutPhillis = false;
        #endregion

        #region TurretBattle
        public bool hasCompletedTurretBattleTraining = false;
        #endregion

        public bool hasSlept = false;
        public bool lostDog = false;
        public bool seenNarwalShip = false;
        public bool isChasingCaptainCoryCenterArea = false;
        public bool isChasingCaptainCoryEngineRoom = false;
        public bool chasedCaptainCoryToEngineRoom = false;

        public PlayerHistory()
        {
            LoadHistory();
        }


        public void LoadHistory()
        {
            //get the type of this class
            Type t = GetType();
            //iterate over this class's fields
            foreach(var p in t.GetFields())
            {
                //load the saved states of this class's fields from PlayerPrefs if there are any saved fields
                if (p.GetValue(this).GetType() == Type.GetType("System.Boolean"))
                {
                    //for booleans
                    if (PlayerPrefs.HasKey(p.Name))
                    {
                        p.SetValue(this, true);
                    }
                    else
                    {
                        p.SetValue(this, false);
                    }
                }
            }
        }



        public void SaveHistory()
        {
            //get the type of this class
            Type t = GetType();
            //iterate over this class's fields
            foreach (var p in t.GetFields())
            {
                //save this class's fields with PlayerPrefs
                if (p.GetValue(this).GetType() == Type.GetType("System.Boolean"))
                {
                    //for booleans
                    if ((bool)p.GetValue(this)) PlayerPrefs.SetInt(p.Name, 1);
                    else PlayerPrefs.DeleteKey(p.Name);
                }
            }
        }
    }
}
