﻿using UnityEngine;
using System.Collections;
using System;
using GSC;
using UnityEngine.UI;

public class Player : GameElement {
    public GameObject scoreObj;
    public PlayerHistory playerHistory;
    public int curSettlementHealth;
    public int maxSettlementHealth;
    private int money;

    // Use this for initialization
    void Start ()
    {
        SetMoney(0);
        curSettlementHealth = maxSettlementHealth;
	}

    public int GetMoney() { return money; }
    public void AddMoney(int money) { SetMoney(this.money + money); }
    public void RemoveMoney(int money) { SetMoney(this.money - money); }
    public void SetMoney(int money)
    {
        this.money = money;
        UpdateMoneyUI();
    }

    void UpdateMoneyUI()
    {
        scoreObj.GetComponent<Text>().text = "$" + money;
    }

    public void TakeDamage(int attackDamage) {
        curSettlementHealth -= attackDamage;
    }

}
