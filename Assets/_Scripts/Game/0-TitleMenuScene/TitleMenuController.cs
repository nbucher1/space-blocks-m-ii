﻿using UnityEngine;
using System.Collections;
using GSC;
using UnityEngine.UI;

public class TitleMenuController : GameSceneController {
    public GameObject initialMenuObj;
    public GameObject inputNameMenuObj;
    public GameObject storyStartObj1;
    public GameObject storyStartObj2;

    // Use this for initialization
    void Start ()
    {
        //ShowInitialMenu();
    }

    public void ShowInitialMenu()
    {
        //displays the initial menu
        initialMenuObj.SetActive(true);
        inputNameMenuObj.SetActive(false);
        storyStartObj1.SetActive(false);
        storyStartObj2.SetActive(false);
    }

    public void ShowInputNameMenu()
    {
        //displays the player name input menu
        initialMenuObj.SetActive(false);
        inputNameMenuObj.SetActive(true);
        storyStartObj1.SetActive(false);
        storyStartObj2.SetActive(false);
    }

    public void ShowStoryStartObj1()
    {
        //start a new game, so clear prefs
        PlayerPrefs.DeleteAll();
        //save the name input
        var playerName = GameObject.Find("PlayerNameInput").GetComponent<Text>().text;
        PlayerPrefs.SetString("PlayerName", playerName);

        //displays the story start object that 
        //   gives a backstory
        storyStartObj1.SetActive(true);
        storyStartObj2.SetActive(false);
    }
    public void ShowStoryStartObj2()
    {
        //displays the story start object that 
        //   gives a backstory
        storyStartObj2.SetActive(true);
    }
}
