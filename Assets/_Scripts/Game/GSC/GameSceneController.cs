﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

namespace GSC
{
    public class GameSceneController : MonoBehaviour
    {
        public enum GameState
        {
            PLAYING,
            IN_DIALOGUE,
            PAUSED  //this doesn't change Time.timeScale and only affects player input in Player.Update()
        }

        public PopupController popupController;
        [HideInInspector]
        public MenuUtility menuUtility;
        [HideInInspector]
        public TimerUtility timerUtility;
        [HideInInspector]
        public Player player;
        [HideInInspector]
        public Transition transition;
        [HideInInspector]
        public GameState curGameState;
        [HideInInspector]
        public MyCamera myCamera;
        [HideInInspector]
        public GameObject canvas;
        [HideInInspector]
        public int numTimesLoaded;
        [HideInInspector]
        public string lastSceneLoaded;
        [HideInInspector]
        public AudioController audioController;
        [HideInInspector]
        public GameStateController gameStateController;
        [HideInInspector]
        public DialogueController dialogueController;
        [HideInInspector]
        public GalagmanautSpawner galagmanautSpawner;
        [HideInInspector]
        public GalagmanautPath galagmanautPath;

        public UnityEvent doOnAwake;


        public bool debugMode = true;


        private T TryToFind<T>() where T : MonoBehaviour
        {
            var obj = FindObjectOfType<T>();
            if (obj == null)
            {
                Debug.Log("Error when trying to find: " + typeof(T));
                return null;
            }
            else
            {
                return obj.GetComponent<T>();
            }
        }

        void DestroyLeftoverAudioSources()
        {
            //if there's audio persisters in the scene
            if (GameObject.FindObjectOfType<AudioPersister>() != null)
            {
                //find the audiosources which don't have audiopersisters(the old ones in the scene) and destroy them 
                var a = GameObject.FindObjectsOfType<AudioSource>().Where(n => n.GetComponent<AudioPersister>() == null);
                foreach (var aSource in a)
                {
                    Destroy(aSource.gameObject);
                }
            }
        }

        void Awake()
        {
            //do all the actions specified in the editor
            doOnAwake.Invoke();

            DestroyLeftoverAudioSources();

            galagmanautPath = TryToFind<GalagmanautPath>();
            audioController = TryToFind<AudioController>();
            galagmanautSpawner = TryToFind<GalagmanautSpawner>();
            canvas = GameObject.Find("Canvas");


            //count the number of times this scene has been loaded
            numTimesLoaded = PlayerPrefs.GetInt("Loaded " + SceneManager.GetActiveScene().name, 0);
            //if we are starting from the editor, set lastSceneLoaded to 0
            if (lastSceneLoaded == SceneManager.GetActiveScene().name && debugMode == true) numTimesLoaded = 0;//<<<<<<<<<<<<<FOR DEBUG PURPOSES
            PlayerPrefs.SetInt("Loaded " + SceneManager.GetActiveScene().name, numTimesLoaded + 1);
            


            var popupControllerObj = GameObject.Find("PopupController");
            if(popupControllerObj != null) popupController = popupControllerObj.GetComponent<PopupController>();
            else Debug.Log("Couldn't find popupControllerObj in the GameSceneController");

            var utilityManagerObj = GameObject.Find("UtilityManager");
            if(utilityManagerObj != null) 
            {
                menuUtility = utilityManagerObj.GetComponent<MenuUtility>();
                timerUtility = utilityManagerObj.GetComponent<TimerUtility>();
                gameStateController = utilityManagerObj.GetComponent<GameStateController>();
                dialogueController = utilityManagerObj.GetComponent<DialogueController>();
            }
            else Debug.Log("Couldn't find utilityManagerObj in the GameSceneController");


            var playerObj = GameObject.Find("Player");
            if(playerObj != null) player = playerObj.GetComponent<Player>();
            else Debug.Log("Couldn't find playerObj in the GameSceneController");


            var transitionObj = GameObject.Find("Transition");
            if(transitionObj != null) transition = transitionObj.GetComponent<Transition>();
            else Debug.Log("Couldn't find playerObj in the GameSceneController");
            

            timerUtility.UnlockControls();      //start each scene with the controls defaulted to unlocked
            curGameState = GameState.PLAYING;   //start each scene with the curGameState defaulted to PLAYING
        }
        
    }
}