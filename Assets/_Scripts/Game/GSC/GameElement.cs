﻿using UnityEngine;
using System.Collections;

namespace GSC
{
    public class GameElement : MonoBehaviour
    {
        [HideInInspector]
        public GameSceneController gsc;
        public Sprite elementIcon;
        public string elementName;
        public string elementDescription;

        protected bool applicationClosing = false;


        protected virtual void Awake()
        {
            if (elementName == null || elementName == "")
            {
                elementName = name;
            }
            gsc = GameObject.Find("GameSceneController").GetComponent<GameSceneController>();
            if (gsc == null) Debug.LogError("Error, GameSceneController not found in GameElement, " + elementName);
        }
        public virtual void SavePrefs()
        {
            ;
        }
        public void OnApplicationQuit()
        {
            applicationClosing = true;
        }
    }
}