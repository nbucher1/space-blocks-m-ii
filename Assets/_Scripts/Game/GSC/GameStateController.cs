﻿using UnityEngine;
using System.Collections;
using GSC;
using UnityEngine.SceneManagement;

public class GameStateController : GameElement {
    //AudioSource pauseSource;
    //AudioSource resumeSource;
    float oldTimeScale = 1;

    // Use this for initialization
    new void Awake()
    {
        base.Awake();
        //get the audiosources for the pausing and resuming
        //pauseSource = gsc.soundPlayer.transform.Find("PauseSound").GetComponent<AudioSource>();
        //resumeSource = gsc.soundPlayer.transform.Find("ResumeSound").GetComponent<AudioSource>();
    }

    public void Pause()
    {
        if (Time.timeScale == 0 && gsc.curGameState == GameSceneController.GameState.PAUSED) return;  //only pause if not paused already
        //pauseSource.Play();
        gsc.curGameState = GameSceneController.GameState.PAUSED;
        oldTimeScale = Time.timeScale;
        Time.timeScale = 0;
    }
    public void Resume()
    {
        if (Time.timeScale == 1 && gsc.curGameState == GameSceneController.GameState.PLAYING) return;  //only resume if not resumed already
        //pauseSource.Stop();
        //resumeSource.Play();
        gsc.curGameState = GameSceneController.GameState.PLAYING;
        Time.timeScale = oldTimeScale;
    }

    public void RestartLevel()
    {
        Resume();
        gsc.audioController.GetComponent<AudioSource>().Stop();
        gsc.transition.LoadLevel(SceneManager.GetActiveScene().name);
    }


    public void ExitLevel()
    {
        Resume();
        gsc.transition.LoadLevel("0-TitleMenu");
        gsc.audioController.GetComponent<AudioSource>().Stop();
    }
    public Menu GameOver()
    {
        //create the menu
        var menu = gsc.menuUtility.CreateEmptyMenu(0, 0);
        menu.shouldPause = true;
        menu.AddText("GAME OVER!", 46, Color.red, FontStyle.Bold);
        //add the buttons
        //menu.AddButton("Restart", RestartLevel);
        //menu.AddButton("Shop", RestartLevel);
        menu.AddButton("OK", ExitLevel);
        Pause();
        return menu;
    }
}
