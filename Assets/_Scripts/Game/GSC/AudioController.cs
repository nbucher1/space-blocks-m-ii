﻿using UnityEngine;
using System.Collections;
using System;
using GSC;

public class AudioController : GameElement {


	void OnEnable () {
        if (GetComponent<AudioPersister>() != null)
        {
            Destroy(gsc.audioController.gameObject);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void RolloffSound(float v)
    {
        StartCoroutine(RolloffSoundCoroutine(v));
    }
    private IEnumerator RolloffSoundCoroutine(float v)
    {
        var audioSource = GetComponent<AudioSource>();
        //make sure that this finishes in time to destroy the sound source and rolloff the sound
        while (audioSource.volume > 0.05f)
        {
            audioSource.volume /= 1.8f;
            yield return new WaitForSeconds(.1f);
        }
    }
}
